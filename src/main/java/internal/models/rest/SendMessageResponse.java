package internal.models.rest;

import com.google.gson.annotations.SerializedName;

public final class SendMessageResponse {
    //region Private fields
    @SerializedName("id")
    private final int messageId;
    private final String message;
    //endregion

    //region Initialization
    public SendMessageResponse(int messageId, String message) {
        this.messageId = messageId;
        this.message = message;
    }
    //endregion

    //region Getters
    public int getMessageId() {
        return messageId;
    }

    public String getMessage() {
        return message;
    }
    //endregion
}
