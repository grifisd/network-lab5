package internal.models.rest;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public final class Message implements Comparable {
    //region Private fields
    @SerializedName("id")
    private final int messageId;
    @SerializedName("message")
    private final String text;
    @SerializedName("author")
    private final int authorId;
    //endregion

    //region Initialization
    public Message(int messageId, String text, int authorId) {
        this.messageId = messageId;
        this.authorId = authorId;
        this.text = text;
    }
    //endregion

    //region Getters
    public int getMessageId() {
        return messageId;
    }

    public String getText() {
        return text;
    }

    public int getAuthorId() {
        return authorId;
    }
    //endregion

    //region Overriden methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return messageId == message.messageId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(messageId);
    }

    @Override
    public int compareTo(Object o) throws NullPointerException {
        if (this == o) throw new NullPointerException();
        if (o == null || getClass() != o.getClass()) throw new NullPointerException();
        Message message = (Message) o;
        return Integer.compare(messageId, message.messageId);
    }

    //endregion
}
