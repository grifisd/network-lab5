package internal.models.rest;

public class FetchMessagesResponse {
    private final Message messages[];

    public FetchMessagesResponse(Message[] messages) {
        this.messages = messages;
    }

    //region Getters
    public Message[] getMessages() {
        return messages;
    }
    //endregion
}
