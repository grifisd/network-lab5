package internal.models.rest;

import com.google.gson.annotations.SerializedName;

public final class LoginResponse {
    //region Private fields
    @SerializedName("id")
    private final int userId;
    private final String username;
    @SerializedName("online")
    private final boolean isOnline;
    private final String token;
    //endregion

    //region Initialization
    public LoginResponse(int userId, String username, boolean isOnline, String token) {
        this.userId = userId;
        this.username = username;
        this.isOnline = isOnline;
        this.token = token;
    }
    //endregion

    //region Getters
    public int getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public String getToken() {
        return token;
    }
    //endregion
}
