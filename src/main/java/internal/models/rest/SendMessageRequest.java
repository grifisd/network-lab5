package internal.models.rest;

public final class SendMessageRequest {
    private final String message;

    public SendMessageRequest(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
