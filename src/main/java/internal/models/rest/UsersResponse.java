package internal.models.rest;

public final class UsersResponse {
    //region Private fields
    private final User users[];
    //endregion

    //region Initializer
    public UsersResponse(User[] users) {
        this.users = users;
    }
    //endregion

    //region Getters
    public User[] getUsers() {
        return users;
    }
    //endregion
}
