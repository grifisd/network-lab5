package internal.models.rest;

public final class LogoutResponse {
    private final String message;

    public LogoutResponse(String message) {
        this.message = message;
    }

    //region Getters
    public String getMessage() {
        return message;
    }
    //endregion
}
