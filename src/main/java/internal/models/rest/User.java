package internal.models.rest;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public final class User {
    //region Private fields
    @SerializedName("id")
    private final int userId;
    private String username;
    @SerializedName("online")
    private Boolean isOnline;
    //endregion

    //region Initialization
    public User(int userId, String username, Boolean isOnline) {
        this.userId = userId;
        this.username = username;
        this.isOnline = isOnline;
    }
    //endregion

    //region Getters and setters
    public int getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public Boolean isOnline() {
        return isOnline;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setOnline(Boolean online) {
        isOnline = online;
    }

    //endregion

    //region Overriden methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(username, user.username);
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", username='" + username + '\'' +
                ", isOnline=" + isOnline +
                '}';
    }
    //endregion
}
