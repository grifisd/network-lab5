package internal.models.rest;

public final class LoginRequest {
    private final String username;

    public LoginRequest(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }
}
