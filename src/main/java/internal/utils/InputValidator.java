package internal.utils;

public class InputValidator {

    public static boolean isPort(String arg) {
        try {
            int level = Integer.parseInt(arg);
            if (level >= 0 && level <= 65535)
                return true;
            else
                return false;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}