package internal.utils;


import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class URIParser {
    public static Map<String, List<String>> getQueryParams(String query) {
        Map<String, List<String>> params = new HashMap<String, List<String>>();
        for (String param : query.split("&")) {
            try {
                String[] pair = param.split("=");
                String key = URLDecoder.decode(pair[0], String.valueOf(StandardCharsets.UTF_8));
                String value = "";
                if (pair.length > 1) {
                    value = URLDecoder.decode(pair[1], String.valueOf(StandardCharsets.UTF_8));
                }

                List<String> values = params.computeIfAbsent(key, k -> new ArrayList<String>());
                values.add(value);
            } catch (Exception ignored) {}
        }
        return params;
    }
}
