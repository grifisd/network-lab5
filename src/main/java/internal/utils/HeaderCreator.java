package internal.utils;

import org.apache.http.Header;
import org.apache.http.message.BasicHeader;

public final class HeaderCreator {

    public static Header contentType() {
        return new BasicHeader("Content-Type", "application/json");
    }

    public static Header usernameIsUsed() {
        return new BasicHeader("WWW-Authenticate", "Token realm='Username is already in use'");
    }

    public static Header authorization(String token) {
        return new BasicHeader("Authorization", token);
    }
}
