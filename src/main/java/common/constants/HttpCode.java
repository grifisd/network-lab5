package common.constants;

public enum HttpCode {
    ok(200), unauthorized(401), forbidden(403), badRequest(400), notFound(404), methodNotAllowed(405), internalError(500);

    private final int rawValue;

    HttpCode(final int code) {
        this.rawValue = code;
    }

    public final int rawValue() {
        return rawValue;
    }
}
