package common.constants;

public final class Queries {
    public static final String root = "/";
    public static final String login = "/login";
    public static final String logout = "/logout";
    public static final String users = "/users";
    public static final String user = "/users/";
    public static final String messages = "/messages";

    public static final class FetchMessages {
        public static final String offset = "offset";
        public static final String count = "count";
        public static final int defaultOffset = 0;
        public static final int defaultCount = 10;
        public static final int maxCount = 100;
    }
}
