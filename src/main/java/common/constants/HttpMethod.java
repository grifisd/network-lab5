package common.constants;

public enum HttpMethod {
    get("GET"), post("POST");

    private final String rawValue;

    HttpMethod(String rawValue) {
        this.rawValue = rawValue;
    }

    public String rawValue() {
        return rawValue;
    }
}
