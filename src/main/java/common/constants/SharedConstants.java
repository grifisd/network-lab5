package common.constants;

public final class SharedConstants {

    //region MainServer params
    public final static int SERVER_BACKLOG = 10;
    public final static int SERVER_THREADS = 10;
    public final static int SERVER_STOP_TIMEOUT = 1;
    public final static int CLIENT_ALIVE_TIMEOUT = 10000; //msec
    public final static int CLIENT_CLEANER_WORKER_TIMEOUT = 3000; //msec
    public final static int webSocketServerPort = 5556;
    //endregion

    //region Client params
    public final static int CLIENT_MESSAGE_FETCH_COUNT = 10;
    public final static int CLIENT_MAX_ERROR_ALLOWED = 10;
    //endregion

    //region Common params
    public final static boolean usingWebSocket = true;
    //endregion
}
