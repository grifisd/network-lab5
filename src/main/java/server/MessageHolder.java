package server;

import internal.models.rest.Message;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public final class MessageHolder {
    //region Singleton methods
    private static volatile MessageHolder instance;

    private MessageHolder() {}

    public static MessageHolder getInstance() {
        MessageHolder localInstance = instance;
        if (localInstance == null) {
            synchronized (MessageHolder.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new MessageHolder();
                }
            }
        }
        return localInstance;
    }
    //endregion

    private List<Message> messages = new CopyOnWriteArrayList<>();
    private int messageIdCount = 0;

    public synchronized Message add(String text, int authorId) {
        try {
            Message message = new Message(messageIdCount++, text, authorId);
            if (!messages.add(message)) {
                throw new Exception();
            }
            return message;
        } catch (Exception e) {
            return null;
        }
    }

    public synchronized Message[] get(int offset, int count) {
        return messages.stream()
                .filter(message ->
                        message.getMessageId() >= offset && message.getMessageId() < offset + count)
                .toArray(Message[]::new);
    }

    public synchronized Message[] get() {
        return messages.toArray(new Message[0]);
    }
}
