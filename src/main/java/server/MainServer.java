package server;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import common.constants.HttpCode;
import internal.models.rest.FetchMessagesResponse;
import internal.models.rest.Message;
import internal.models.rest.User;
import internal.utils.InputValidator;
import common.constants.Queries;
import common.constants.SharedConstants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import server.client.Client;
import server.client.ClientHolder;
import server.handlers.errorHandlers.BadRequestHandler;
import server.handlers.requestHandlers.*;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.Base64;
import java.util.Scanner;
import java.util.concurrent.Executors;

import org.java_websocket.WebSocket;

/**
 * Retreives listening port from args
 */

public class MainServer {
    public static void main(String[] args) {
        //region Check args
        if (args.length != 1 || !InputValidator.isPort(args[0])) {
            printUsage();
            System.exit(0);
        }
        //endregion

        MessagesServer s = new MessagesServer(new InetSocketAddress("localhost", SharedConstants.webSocketServerPort));
        s.start();

        //region Configure Sun HttpServer
        HttpServer httpServer = null;
        try {
            httpServer = HttpServer.create(
                    new InetSocketAddress(Integer.parseInt(args[0])),
                    SharedConstants.SERVER_BACKLOG
            );
        } catch (IOException e) {
            System.err.println("server could not started. Exiting.");
            System.exit(1);
        }

        httpServer.createContext(Queries.root, new BadRequestHandler());
        httpServer.createContext(Queries.login, new LoginHandler());
        httpServer.createContext(Queries.logout, new LogoutHandler());
        httpServer.createContext(Queries.users, new UsersHandler());
        httpServer.createContext(Queries.user, new UserHandler());
        httpServer.createContext(Queries.messages, new SendMessageHandler());

        httpServer.setExecutor(Executors.newFixedThreadPool(
                SharedConstants.SERVER_THREADS));

        httpServer.start();
        System.out.println("MainServer started.");
        //endregion

//        ClientCleaner cleaner = new ClientCleaner();
//        cleaner.start();

        //region Interactive shell
        Scanner in = new Scanner(System.in);
        while (true) {
            String userInput = in.nextLine();
            switch (userInput) {
                case "/stop":
                    System.out.println(
                            "server will stop in "
                                    + SharedConstants.SERVER_STOP_TIMEOUT
                                    + " seconds.");
                    httpServer.stop(SharedConstants.SERVER_STOP_TIMEOUT);
                    System.exit(0);
                    break;
                case "/restart":
                    System.out.println("Not implemented yet.");
                    break;
//                    StringBuilder cmd = new StringBuilder();
//                    cmd.append(System.getProperty("java.home") + File.separator + "bin" + File.separator + "java ");
//                    for (String jvmArg : ManagementFactory.getRuntimeMXBean().getInputArguments()) {
//                        cmd.append(jvmArg + " ");
//                    }
//                    cmd.append("-cp ").append(ManagementFactory.getRuntimeMXBean().getClassPath()).append(" ");
//                    cmd.append(MainMainServer.class.getName()).append(" ");
//                    for (String arg : args) {
//                        cmd.append(arg).append(" ");
//                    }
//                    try {
//                        System.out.println("Cmd: " + cmd.toString());
//                        Runtime.getRuntime().exec(cmd.toString());
//                    } catch (IOException e) {
//                        System.err.println("Cannot restart server. Exiting.");
//                    }
//                    System.exit(0);
//                    break;
                case "/users":
                    User[] users =  ClientHolder.getInstance().getUsers();
                    for (User user : users) {
                        System.out.println(user);
                    }
                    break;
                default:
                    System.out.println("\"/stop\" or \"/restart\" or \"/users\"");
            }

        }
        //endregion
    }

    private static void printUsage() {
        System.out.println("Usage: java server LOCALPORT");
        System.out.println("Where:");
        System.out.println("\tLOCALPORT - port on this machine " +
                "which will be used. Integer between 0-65535");
    }
}

class MessagesServer extends WebSocketServer {
    private static final Logger log = LogManager.getLogger(MessagesServer.class);

    MessagesServer(InetSocketAddress address) {
        super(address);
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        log.trace("onMessage.String from "	+ conn.getRemoteSocketAddress() + ": " + message);
        Message msg = MessageHolder.getInstance().add(message, ((Client)conn.getAttachment()).getRelatedUser().getUserId());
        broadcast("["
                + msg.getAuthorId()
                + "]: "
                + msg.getText());
    }

    @Override
    public void onMessage(WebSocket conn, ByteBuffer message) {
        log.trace("onMessage.ByteBuffer from "	+ conn.getRemoteSocketAddress());
        byte[] bytes = new byte[message.remaining()];
        message.get(bytes);
        log.trace("onMessage.ByteBuffer in Base64: " + Base64.getEncoder().encodeToString(bytes));

        String token = new String(bytes);
        Client client;
        if (null == (client = ClientHolder.getInstance().getClientByToken(token))) {
            conn.close(HttpCode.unauthorized.rawValue());
            return;
        }

        conn.setAttachment(client);

        //send all saved messages to this client
        for (Message msg : MessageHolder.getInstance().get()) {
            conn.send("["
                    + msg.getAuthorId()
                    + "]: "
                    + msg.getText());
        }
    }

    //region Debuggable not used overriden methods
    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        log.trace("New connection to " + conn.getRemoteSocketAddress());
    }

    @Override
    public void onStart() {
        System.out.println("WebSocketServer started successfully");
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        log.trace("Closed " + conn.getRemoteSocketAddress() + " with exit code " + code + " additional info: " + reason);
        ClientHolder.getInstance().unregisterClient(conn.getAttachment());
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {
        log.error("An error occured on connection " + (null == conn ? "<null>" : conn.getRemoteSocketAddress())  + ":" + ex);
        System.err.println("Some WebSocket error occured. Exiting.");
        System.exit(1);
        //conn.close(HttpCode.internalError.rawValue());
    }
    //endregion
}