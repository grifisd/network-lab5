package server;

import common.constants.SharedConstants;
import server.client.Client;
import server.client.ClientHolder;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class ClientCleaner extends Thread {
    ClientCleaner() {
        super();
    }

    @Override
    public void run() {
        while (true) {
            Client[] clients = ClientHolder.getInstance().getClients();
            Calendar now = new GregorianCalendar();
            now.add(Calendar.MILLISECOND, -1*SharedConstants.CLIENT_ALIVE_TIMEOUT);
            for (Client client : clients) {
                if (client.getLastKeepAlive().before(now)) {
                    ClientHolder.getInstance().unregisterClient(client);
                }
            }
            try {
                Thread.sleep(SharedConstants.CLIENT_CLEANER_WORKER_TIMEOUT);
            } catch (InterruptedException ignored) {}
        }
    }
}
