package server.handlers.errorHandlers;

import common.constants.HttpCode;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;

public class NotFoundHandler implements HttpHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        try {
            httpExchange.sendResponseHeaders(HttpCode.notFound.rawValue(), -1);
        } catch (IOException e) {
            System.err.println("Cannot send NotFound to client");
        }
    }
}
