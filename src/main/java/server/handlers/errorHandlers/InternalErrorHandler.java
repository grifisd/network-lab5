package server.handlers.errorHandlers;

import common.constants.HttpCode;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;

public class InternalErrorHandler implements HttpHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        try {
            httpExchange.sendResponseHeaders(HttpCode.internalError.rawValue(), -1);
            httpExchange.close();
        } catch (IOException e) {
            System.err.println("Cannot send InternalServerError code to client");
        }
    }
}
