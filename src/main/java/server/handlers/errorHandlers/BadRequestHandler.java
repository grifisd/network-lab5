package server.handlers.errorHandlers;

import common.constants.HttpCode;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;

public class BadRequestHandler implements HttpHandler {
    @Override
    public void handle(HttpExchange httpExchange) {
        try {
            httpExchange.sendResponseHeaders(HttpCode.badRequest.rawValue(), -1);
            httpExchange.close();
        } catch (IOException e) {
            System.err.println("Cannot send Bad Request to " + httpExchange.getRemoteAddress());
        }
    }
}
