package server.handlers.errorHandlers;

import common.constants.HttpCode;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;

public class BadMethodHandler implements HttpHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        httpExchange.sendResponseHeaders(HttpCode.methodNotAllowed.rawValue(), -1);
        httpExchange.close();
    }
}
