package server.handlers.requestHandlers;

import common.constants.HttpMethod;
import server.client.Client;
import server.handlers.errorHandlers.*;
import server.client.ClientHolder;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

public abstract class BasicHandler {

    /**
     * Checks header for Content-Type:application/json, deserializates json-body,
     * sends BadRequest and throws IOException in case of any error
     * @param resultType Class<Type> of the request JSON object which corresponds to some model
     * @param httpExchange this httpExchange
     * @return ResultType - deserialized object
     * @throws IOException in case of any problems to terminate children's handle() method
     */
    public <ResultType> ResultType getBodyObject(
            Class<ResultType> resultType,
            HttpExchange httpExchange) throws JsonParseException {
        //firstly check for header
        if (!containsJsonHeader(httpExchange.getRequestHeaders())) {
            new BadRequestHandler().handle(httpExchange);
            throw new JsonParseException("No application/json header");
        }

        Reader reader = new BufferedReader(new InputStreamReader(httpExchange.getRequestBody()));
        Gson g = new GsonBuilder().serializeNulls().create();
        ResultType mappedObject;
        try {
            mappedObject = g.fromJson(reader, resultType);
            if (null == mappedObject) {
                throw new JsonParseException("Unwrapped object seems to be null");
            }
        } catch (JsonParseException e) {
            new BadRequestHandler().handle(httpExchange);
            throw new JsonParseException("Cannot deserialize object");
        }

        return mappedObject;
    }


    /**
     * Checks http method for specified one, deserializates json-body,
     * sends BadRequest and throws IOException in case of any error
     *
     * @param httpExchange current exchange with client
     * @param httpMethod method to check. See {@link HttpMethod} for more
     * @throws IOException in case of any problems to terminate children's handle() method
     */
    void verifyHttpMethod(HttpExchange httpExchange, HttpMethod httpMethod) throws IOException {
        if (!httpExchange.getRequestMethod().equalsIgnoreCase(httpMethod.rawValue())) {
            new BadMethodHandler().handle(httpExchange);
            throw new IOException();
        }
    }

    /**
     * Checks for token is good, if its bad send Unauthorized/Forbidden and break children's handle method
     *
     * @param httpExchange current exchange with client
     * @return client client if found any valid token which corresponds to some client
     * @throws IOException throws in case of token is not found or is not valid
     */
    Client verifyToken(HttpExchange httpExchange) throws IOException {
        List<String> tokens = httpExchange.getRequestHeaders().get("Authorization");
        //firstly check if there no tokens
        if (tokens == null || tokens.size() == 0) {
            new UnauthorizedHandler().handle(httpExchange);
            throw new IOException(); //to exit on child handler
        }
        ClientHolder holder = ClientHolder.getInstance();
        //logout all of them
        for (String token : tokens) {
            Client validToken;
            if (null != (validToken = holder.getClientByToken(token))) {
                return validToken;
            }
        }
        //if there no any tokens which corresponds to some client, send Forbidden
        new ForbiddenHandler().handle(httpExchange);
        throw new IOException(); //to exit on child handler
    }



    //region JSON header workers
    private boolean containsJsonHeader(Headers headers) {
        return headers.get("Content-Type").contains("application/json");
    }

    void addJsonHeader(Headers headers) {
        headers.add("Content-Type", "application/json");
    }
    //endregion
}
