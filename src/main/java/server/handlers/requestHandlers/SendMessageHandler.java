package server.handlers.requestHandlers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import common.constants.HttpCode;
import common.constants.HttpMethod;
import internal.models.rest.Message;
import internal.models.rest.SendMessageRequest;
import internal.models.rest.SendMessageResponse;
import server.client.Client;
import server.handlers.errorHandlers.BadRequestHandler;
import server.handlers.errorHandlers.InternalErrorHandler;
import server.MessageHolder;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;

public class SendMessageHandler extends BasicHandler implements HttpHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        if (httpExchange.getRequestMethod().equalsIgnoreCase(HttpMethod.get.rawValue())) {
            new FetchMessagesHandler().handle(httpExchange);
            return;
        }
        verifyHttpMethod(httpExchange, HttpMethod.post);
        Client client = verifyToken(httpExchange);
        SendMessageRequest request = getBodyObject(SendMessageRequest.class, httpExchange);

        if (null == request.getMessage()) {
            new BadRequestHandler().handle(httpExchange);
            return;
        }

        MessageHolder messageHolder = MessageHolder.getInstance();
        Message message;
        if (null == (message = messageHolder.add(
                request.getMessage(),
                client.getRelatedUser().getUserId()))) {
            new InternalErrorHandler().handle(httpExchange);
            return;
        }

        SendMessageResponse response = new SendMessageResponse(message.getMessageId(), message.getText());

        Gson g = new GsonBuilder().serializeNulls().create();
        byte[] responseBodyBytes = g.toJson(response).getBytes();

        addJsonHeader(httpExchange.getResponseHeaders());
        httpExchange.sendResponseHeaders(
                HttpCode.ok.rawValue(),
                responseBodyBytes.length);
        httpExchange.getResponseBody().write(responseBodyBytes);
        httpExchange.close();
    }
}
