package server.handlers.requestHandlers;

import common.constants.HttpCode;
import common.constants.HttpMethod;
import internal.models.rest.LoginRequest;
import internal.models.rest.LoginResponse;
import server.client.Client;
import server.client.ClientHolder;
import server.handlers.errorHandlers.BadRequestHandler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;

public class LoginHandler extends BasicHandler implements HttpHandler{

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        verifyHttpMethod(httpExchange, HttpMethod.post);
        LoginRequest request = getBodyObject(LoginRequest.class, httpExchange);

        if (null == request.getUsername()) {
            new BadRequestHandler().handle(httpExchange);
            return;
        }

        ClientHolder holder = ClientHolder.getInstance();
        Client client;
        if (null == (client = holder.registerClientByUsername(request.getUsername()))) {
            //there was someone with the same username
            Headers headers = httpExchange.getResponseHeaders();
            headers.add("WWW-Authenticate", "Token realm='Username is already in use'");
            httpExchange.sendResponseHeaders(HttpCode.unauthorized.rawValue(), -1);
            return;
        }
        addJsonHeader(httpExchange.getResponseHeaders());
        LoginResponse response = new LoginResponse(
                client.getRelatedUser().getUserId(),
                client.getRelatedUser().getUsername(),
                client.getRelatedUser().isOnline(),
                client.getToken());
        Gson g = new GsonBuilder().serializeNulls().create();
        byte[] responseBodyBytes = g.toJson(response).getBytes();

        httpExchange.sendResponseHeaders(
                HttpCode.ok.rawValue(),
                responseBodyBytes.length);
        httpExchange.getResponseBody().write(responseBodyBytes);
        httpExchange.close();
    }
}
