package server.handlers.requestHandlers;

import common.constants.HttpCode;
import common.constants.HttpMethod;
import server.client.Client;
import server.client.ClientHolder;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;

public class LogoutHandler extends BasicHandler implements HttpHandler {
    private final static byte[] logoutResponse = "{\"message\":\"bye\"}".getBytes();

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        verifyHttpMethod(httpExchange, HttpMethod.post);
        Client client = verifyToken(httpExchange);
        ClientHolder holder = ClientHolder.getInstance();
        holder.unregisterClient(client);
        //then send response
        addJsonHeader(httpExchange.getResponseHeaders());
        httpExchange.sendResponseHeaders(HttpCode.ok.rawValue(), logoutResponse.length);
        httpExchange.getResponseBody().write(logoutResponse);
        httpExchange.close();
    }
}
