package server.handlers.requestHandlers;

import common.constants.HttpCode;
import common.constants.HttpMethod;
import internal.models.rest.User;
import server.client.ClientHolder;
import server.handlers.errorHandlers.BadRequestHandler;
import server.handlers.errorHandlers.NotFoundHandler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.net.URI;

public class UserHandler extends BasicHandler implements HttpHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        verifyHttpMethod(httpExchange, HttpMethod.get);
        verifyToken(httpExchange);
        URI path = httpExchange.getRequestURI();

        String[] tokens = path.getPath().split("/");
        if (tokens.length != 3) {
            new BadRequestHandler().handle(httpExchange);
            return;
        }
        int userId;

        try {
            userId = Integer.parseInt(tokens[2]);
        } catch (NumberFormatException e) {
            new BadRequestHandler().handle(httpExchange);
            return;
        }

        ClientHolder holder = ClientHolder.getInstance();
        User responseUser = holder.getUserById(userId);
        if (null == responseUser) {
            new NotFoundHandler().handle(httpExchange);
            return;
        }

        Gson g = new GsonBuilder().serializeNulls().create();
        byte[] responseBodyBytes = g.toJson(responseUser).getBytes();

        addJsonHeader(httpExchange.getResponseHeaders());
        httpExchange.sendResponseHeaders(
                HttpCode.ok.rawValue(),
                responseBodyBytes.length);
        httpExchange.getResponseBody().write(responseBodyBytes);
        httpExchange.close();
    }
}
