package server.handlers.requestHandlers;

import common.constants.HttpCode;
import common.constants.HttpMethod;
import internal.models.rest.User;
import internal.models.rest.UsersResponse;
import server.client.ClientHolder;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;

public class UsersHandler extends BasicHandler implements HttpHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        verifyHttpMethod(httpExchange, HttpMethod.get);
        verifyToken(httpExchange);
        ClientHolder holder = ClientHolder.getInstance();
        User[] users = holder.getUsers();

        UsersResponse response = new UsersResponse(users);
        Gson g = new GsonBuilder().serializeNulls().create();
        byte[] responseBodyBytes = g.toJson(response).getBytes();

        httpExchange.sendResponseHeaders(
                HttpCode.ok.rawValue(),
                responseBodyBytes.length);
        httpExchange.getResponseBody().write(responseBodyBytes);
        httpExchange.close();

    }
}
