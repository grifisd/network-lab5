package server.handlers.requestHandlers;

import common.constants.HttpCode;
import common.constants.HttpMethod;
import common.constants.Queries;
import internal.models.rest.FetchMessagesResponse;
import internal.models.rest.Message;
import internal.utils.URIParser;
import server.client.Client;
import server.handlers.errorHandlers.BadRequestHandler;
import server.MessageHolder;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class FetchMessagesHandler extends BasicHandler implements HttpHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        verifyHttpMethod(httpExchange, HttpMethod.get);
        Client client = verifyToken(httpExchange);
        client.updateKeepAlive();


        int offset;
        int count;
        try {
            Map<String, List<String>> params =
                    URIParser.getQueryParams(httpExchange.getRequestURI().getQuery());
            try {
                offset = Integer.parseInt(params.get(Queries.FetchMessages.offset).get(0));
            } catch (Exception e) {
                offset = Queries.FetchMessages.defaultOffset;
            }
            try {
                count = Integer.parseInt(params.get(Queries.FetchMessages.count).get(0));
            } catch (Exception e) {
                count = Queries.FetchMessages.defaultCount;
            }
            if (offset < 0
                    || count > Queries.FetchMessages.maxCount
                    || count < 0) {
                new BadRequestHandler().handle(httpExchange);
            }
        } catch (Exception e) {
            offset = Queries.FetchMessages.defaultOffset;
            count = Queries.FetchMessages.defaultCount;
        }
        MessageHolder messageHolder = MessageHolder.getInstance();
        Message[] messages = messageHolder.get(offset, count);
        FetchMessagesResponse response = new FetchMessagesResponse(messages);

        Gson g = new GsonBuilder().serializeNulls().create();
        byte[] responseBodyBytes = g.toJson(response).getBytes();

        addJsonHeader(httpExchange.getResponseHeaders());
        httpExchange.sendResponseHeaders(
                HttpCode.ok.rawValue(),
                responseBodyBytes.length);
        httpExchange.getResponseBody().write(responseBodyBytes);
        httpExchange.close();
    }
}
