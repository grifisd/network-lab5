package server.client;

import internal.models.rest.User;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Client {
    //region Private fields
    private final User relatedUser;
    private final String token;
    private Calendar lastKeepAlive;
    //endregion

    //region Initialization
    Client(User relatedUser, String token, Calendar lastKeepAlive) {
        this.relatedUser = relatedUser;
        this.token = token;
        this.lastKeepAlive = lastKeepAlive;
    }
    //endregion

    //region Getters
    public User getRelatedUser() {
        return relatedUser;
    }

    public String getToken() {
        return token;
    }

    public Calendar getLastKeepAlive() {
        return lastKeepAlive;
    }

    //endregion

    public void updateKeepAlive() {
        lastKeepAlive = new GregorianCalendar();
    }
}
