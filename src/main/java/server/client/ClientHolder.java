package server.client;
import internal.models.rest.User;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class ClientHolder {
    //region Singleton methods
    private static volatile ClientHolder instance;

    private ClientHolder() {}

    public static ClientHolder getInstance() {
        ClientHolder localInstance = instance;
        if (localInstance == null) {
            synchronized (ClientHolder.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new ClientHolder();
                }
            }
        }
        return localInstance;
    }
    //endregion

    private final List<Client> clients = new CopyOnWriteArrayList<Client>();
    private int userIdCount = 0;

    private synchronized boolean isUsernameValid(String username) {
        if (null == username)
            return false;

        return clients.stream().noneMatch(client ->
                client.getRelatedUser().getUsername().equals(username));
    }

    public synchronized Client registerClientByUsername(String username) {
        if (!isUsernameValid(username)) {
            return null;
        }
        User user = new User(userIdCount++, username, true);
        Client client = new Client(user, UUID.randomUUID().toString(), new GregorianCalendar());

        if (clients.add(client)) {
            return client;
        }
        return null;
    }

    public synchronized boolean unregisterClient(Client client) {
        return clients.remove(client);
    }

    /**
     * Searches for suitable {@link Client} object
     *
     * @param token token given from client application
     * @return {@link Client} object associated with given token or null if there no any
     */
    public synchronized Client getClientByToken(String token) {
        try {
            return clients
                    .stream()
                    .filter(client ->
                            client.getToken().equals(token))
                    .findAny().get();
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    public synchronized User getUserById(int id) {
        try {
            return clients.stream()
                    .filter(client ->
                            client.getRelatedUser().getUserId() == id)
                    .findAny().get().getRelatedUser();
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    public synchronized Client[] getClients() {
        return clients.toArray(new Client[0]);
    }

    public synchronized User[] getUsers() {
        User[] users = new User[clients.size()];
        int i = 0;
        for (Client client : clients) {
            users[i++] = client.getRelatedUser();
        }
        return users;
    }
}
