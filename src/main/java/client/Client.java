package client;

import client.requests.Requester;
import common.constants.SharedConstants;
import internal.models.rest.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.util.Base64;
import java.util.Optional;
import java.util.Scanner;

public class Client {
    //region Private fields
    private static final Logger log = LogManager.getLogger(Client.class);
    private static Scanner scanner = new Scanner(System.in);
    private static Requester requester;
    private static String token;
    private static int lastFetchedMessageId = 0;
    private static int errorCount = 0;
    private static WebSocketClient client;
    //endregion

    public static void main(String[] args) {
        //region Args check
        if (args.length != 2) {
            printUsage();
            System.exit(0);
        }
        URI serverUri = null;
        try {
            serverUri = new URI(
                    "http",
                    null,
                    args[0],
                    Integer.parseInt(args[1]),
                    null,
                    null,
                    null);
        } catch (Exception e) {
            printUsage();
            System.exit(0);
        }
        //endregion
        requester = new Requester(serverUri);

        login();

        if (SharedConstants.usingWebSocket) {
            //region WebSocket fetching messages
            try {
                client = new MessagesClient(
                        new URI("ws://localhost:" + SharedConstants.webSocketServerPort +"/messages"),
                        token);
            } catch (URISyntaxException e) {
                log.fatal(e);
            }
            client.connect();
            //endregion
        } else {
            //region Message polling thread
            new Thread(() -> {
                while (true) {
                    fetchMessages(SharedConstants.CLIENT_MESSAGE_FETCH_COUNT);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        System.err.println("Message Fetcher was interrupted");
                    }
                }
            }).start();
            //endregion
        }

        //region Interactive shell
        while (true) {
            String userInput = scanner.nextLine();
            switch (userInput) {
                case "/list":
                    fetchUsers();
                    break;
                case "/logout":
                    logout();
                default:
                    //sendMessage(userInput);
                    client.send(userInput);

            }
        }
        //endregion
    }

    //region Private methods
    private static void login() {
        Optional<LoginResponse> response;
        String username;
        do {
            System.out.print("Please input your username: ");
            username = scanner.nextLine();
            response = requester.doLoginRequest(username);
        } while (!response.isPresent());
        token = response.get().getToken();
        System.out.println("Successfully logged in as '" + username + "'!");
    }

    private static void logout() {
        Optional<LogoutResponse> response = requester.doLogoutRequest(token);
        if (!response.isPresent()) {
            System.out.println("Cannot logout from server. Just exiting");
            System.exit(1);
        }
        LogoutResponse unwrapped = response.get();
        System.out.println(unwrapped.getMessage());
        System.exit(0);
    }

    private static void fetchMessages(int count) {
        while (true) {
            Optional<FetchMessagesResponse> response =
                    requester.doFetchMessagesRequest(
                            token,
                            lastFetchedMessageId,
                            count);
            if (!response.isPresent()) {
                System.err.println("Cannot fetch messages");
                errorCheck();
                return;
            }
            for (Message message : response.get().getMessages()) {
                System.out.println(
                        "["
                        + message.getAuthorId()
                        + "]: "
                        + message.getText());
            }

            lastFetchedMessageId += response.get().getMessages().length;
            if (response.get().getMessages().length < count) {
                //that means that we got all available messages and we need to return
                return;
            }
        }
    }

    private static void fetchUsers() {
        Optional<UsersResponse> response = requester.doFetchUsersRequest(token);
        if (!response.isPresent()) {
            System.err.println("Cannot fetch users");
            return;
        }
        User[] users = response.get().getUsers();
        System.out.println("ID\tUsername\tIs online");
        for (User user: users) {
            System.out.println(
                    user.getUserId()
                    + "\t"
                    + user.getUsername()
                    + "\t\t"
                    + user.isOnline());
        }
    }

    private static void sendMessage(String text) {
        Optional<SendMessageResponse> response = requester.doSendMessageRequest(token, text);
        if (!response.isPresent()) {
            System.err.println("Cannot send message to the server.");
            errorCheck();
            return;
        }
        SendMessageResponse messageResponse = response.get();
        //System.out.println("Send: " + messageResponse.getMessageId() + ": " + messageResponse.getMessage());
    }

    private static void printUsage() {
        System.out.println("Usage: java client SERVER_IP SERVER_PORT");
        System.out.println("Where:");
        System.out.println("\tSERVER_IP - ip address of hosting server");
        System.out.println("\tSERVER_PORT - port of the server to use");
    }

    private static void errorCheck() {
        if (errorCount++ > SharedConstants.CLIENT_MAX_ERROR_ALLOWED) {
            System.err.println("Too many errors happened. Exiting.");
            System.exit(1);
        }
    }
    //endregion
}

class MessagesClient extends WebSocketClient {
    //region Private fields
    private static final Logger log = LogManager.getLogger(MessagesClient.class);
    private final String token;
    //endregion

    //region Initializers
    public MessagesClient(URI serverUri, Draft draft, String token) {
        super(serverUri, draft);
        this.token = token;
    }

    public MessagesClient(URI serverURI, String token) {
        super(serverURI);
        this.token = token;
    }
    //endregion

    @Override
    public void onOpen(ServerHandshake handshake) {
        log.trace("onOpen");
        //auth
        send(token.getBytes());
    }

    @Override
    public void onMessage(String message) {
        log.trace("onMessage.String:" + message);
        System.out.println(message);
    }

    //region Debuggable overriden methods
    @Override
    public void onClose(int code, String reason, boolean remote) {
        log.trace("Closed with exit code " + code + " additional info: " + reason);
    }

    @Override
    public void onMessage(ByteBuffer message) {
        byte[] bytes = new byte[message.remaining()];
        message.get(bytes);
        log.trace("onMessage.ByteBuffer in Base64: " + Base64.getEncoder().encodeToString(bytes));
    }

    @Override
    public void onError(Exception ex) {
        log.error("onError: " + ex);
        System.err.println("Websocket error occured. Exiting");
        System.exit(1);
    }
    //endregion
}