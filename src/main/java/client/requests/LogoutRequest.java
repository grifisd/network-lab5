package client.requests;

import common.constants.HttpMethod;
import common.constants.Queries;
import internal.utils.HeaderCreator;
import org.apache.http.Header;

import java.net.URI;

class LogoutRequest extends BaseRequest {
    private final String token;

    LogoutRequest(URI baseUri, String token) {
        super(baseUri);
        this.token = token;
    }

    @Override
    Header[] headers() {
        return new Header[]{HeaderCreator.authorization(token)};
    }

    @Override
    String path() {
        return Queries.logout;
    }

    @Override
    HttpMethod method() {
        return HttpMethod.post;
    }

    @Override
    byte[] body() {
        return null;
    }

    @Override
    String query() {
        return null;
    }
}
