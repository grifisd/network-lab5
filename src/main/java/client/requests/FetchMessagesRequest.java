package client.requests;

import common.constants.HttpMethod;
import common.constants.Queries;
import internal.utils.HeaderCreator;
import org.apache.http.Header;

import java.net.URI;

class FetchMessagesRequest extends BaseRequest {
    private final String token;
    private final int offset;
    private final int count;

    FetchMessagesRequest(URI baseUri, String token, int offset, int count) {
        super(baseUri);
        this.token = token;
        this.offset = offset;
        this.count = count;
    }

    @Override
    Header[] headers() {
        return new Header[] {
                HeaderCreator.contentType(),
                HeaderCreator.authorization(token)
        };
    }

    @Override
    String path() {
        return Queries.messages;
    }

    @Override
    HttpMethod method() {
        return HttpMethod.get;
    }

    @Override
    byte[] body() {
        return null;
    }

    @Override
    String query() {
        return "offset=" + offset
                + "&"
                +"count=" + count;
    }
}
