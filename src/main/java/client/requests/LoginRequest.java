package client.requests;

import com.google.gson.Gson;
import common.constants.HttpMethod;
import common.constants.Queries;
import internal.utils.HeaderCreator;
import org.apache.http.Header;

import java.net.URI;

class LoginRequest extends BaseRequest {
    private final internal.models.rest.LoginRequest model;

    LoginRequest(URI baseUri, internal.models.rest.LoginRequest model) {
        super(baseUri);
        this.model = model;
    }

    @Override
    Header[] headers() {
        return new Header[]{HeaderCreator.contentType()};
    }

    @Override
    String path() {
        return Queries.login;
    }

    @Override
    HttpMethod method() {
        return HttpMethod.post;
    }

    @Override
    byte[] body() {
        Gson g = new Gson();
        return g.toJson(model).getBytes();
    }

    @Override
    String query() {
        return null;
    }
}