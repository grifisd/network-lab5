package client.requests;

import com.google.gson.Gson;
import common.constants.HttpMethod;
import common.constants.Queries;
import internal.utils.HeaderCreator;
import org.apache.http.Header;

import java.net.URI;

class SendMessageRequest extends BaseRequest {
    private String token;
    private internal.models.rest.SendMessageRequest message;

    SendMessageRequest(URI baseUri, String token, internal.models.rest.SendMessageRequest message) {
        super(baseUri);
        this.message = message;
        this.token = token;
    }

    @Override
    Header[] headers() {
        return new Header[] {
                HeaderCreator.authorization(token),
                HeaderCreator.contentType()
        };
    }

    @Override
    String path() {
        return Queries.messages;
    }

    @Override
    HttpMethod method() {
        return HttpMethod.post;
    }

    @Override
    byte[] body() {
        Gson g = new Gson();
        return g.toJson(message).getBytes();
    }

    @Override
    String query() {
        return null;
    }
}
