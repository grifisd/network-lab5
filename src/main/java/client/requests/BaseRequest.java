package client.requests;

import common.constants.HttpMethod;
import org.apache.http.*;

import java.net.URI;

abstract class BaseRequest {
    private final URI baseUri;

    BaseRequest(URI baseUri) {
        this.baseUri = baseUri;
    }

    final URI baseUri() { return baseUri; }
    abstract Header[] headers();
    abstract String path();
    abstract HttpMethod method();
    abstract byte[] body();
    abstract String query();
}