package client.requests;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import internal.models.rest.*;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.fluent.Request;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

public class Requester {
    private final Gson jsonSerializer = new GsonBuilder().serializeNulls().create();
    private final URI serverUri;

    public Requester(URI serverUri) {
        this.serverUri = serverUri;
    }

    /**
     * @param request Object which inherits of {@link BaseRequest}
     * @return {@link Optional} of {@link HttpResponse}. Value is not present only in case when IO error occurred:
     * bad connection, server refused connection or server is not started
     */
    private Optional<HttpResponse> execute(BaseRequest request) {
        URI uri;
        try {
            if (null != request.query())
                uri = new URI(
                        request.baseUri()
                            + request.path()
                            + "?"
                            + request.query());
            else
                uri = new URI(request.baseUri() + request.path());
        } catch (URISyntaxException e) {
            return Optional.empty();
        }
        Request apacheRequest = null;
        switch (request.method()) {
            case get:
                apacheRequest = Request.Get(uri);
                break;
            case post:
                apacheRequest = Request.Post(uri);
                break;
        }
        apacheRequest.setHeaders(request.headers());
        if (null != request.body()) {
            apacheRequest.bodyByteArray(request.body());
        }
        try {
            HttpResponse response = apacheRequest.execute().returnResponse();
            if (null == response)
                return Optional.empty();
            return Optional.of(response);
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    /**
     * Basically processes given response. Only checks for HTTP 200 and unwraps body-json
     *
     * @param response Wrapped {@link HttpResponse}
     * @param responseTypeClass Class of ResponseType
     * @param <ResponseType> Type of model to unwrap JSON
     * @return {@link Optional} of {@link HttpResponse}. Value is not presented only
     * in case of bad JSON or bad HTTP code
     */
    private <ResponseType> Optional<ResponseType> baseProcess(
            Optional<HttpResponse> response,
            Class<ResponseType> responseTypeClass) {
        if (!response.isPresent()) {
            System.err.println("Cannot connect to server.");
            return Optional.empty();
        }
        try {
            HttpResponse httpResponse = response.get();
            switch (httpResponse.getStatusLine().getStatusCode()) {
                case HttpStatus.SC_OK:
                    ResponseType responseUnwrapped = jsonSerializer.fromJson(
                            new InputStreamReader(httpResponse.getEntity().getContent()),
                            responseTypeClass);
                    return Optional.of(responseUnwrapped);
                default:
                    System.err.println(
                            "Some error happened: server returned "
                                    + httpResponse.getStatusLine());
                    //System.err.println("Exiting.");
                    //System.exit(1);
                    return Optional.empty();

            }
        } catch (IOException e) {
            System.err.println("Some client error happened: " + e);
            return Optional.empty();
        }
    }

    public Optional<LoginResponse> doLoginRequest(String username) {
        internal.models.rest.LoginRequest loginRequestModel = new internal.models.rest.LoginRequest(username);
        LoginRequest request = new LoginRequest(serverUri, loginRequestModel);
        Optional<HttpResponse> response = execute(request);

        if (!response.isPresent()) {
            System.err.println("Cannot connect to server.");
            return Optional.empty();
        }

        try {
            HttpResponse httpResponse = response.get();
            switch (httpResponse.getStatusLine().getStatusCode()) {
                case HttpStatus.SC_OK:
                    LoginResponse loginResponse = jsonSerializer.fromJson(
                            new InputStreamReader(httpResponse.getEntity().getContent()),
                            LoginResponse.class);
                    return Optional.of(loginResponse);
                case HttpStatus.SC_UNAUTHORIZED:
                    System.out.println("Username already in use. Try another one.");
                    return Optional.empty();
                default:
                    System.err.println(
                            "Some error happened: server returned "
                                    + httpResponse.getStatusLine());
                    System.err.println("Exiting.");
                    System.exit(1);
                    return Optional.empty();

            }
        } catch (IOException e) {
            System.err.println("Some client error happened: " + e);
            return Optional.empty();
        }
    }

    public Optional<FetchMessagesResponse> doFetchMessagesRequest(String token, int offset, int count) {
        FetchMessagesRequest request = new FetchMessagesRequest(serverUri, token, offset, count);
        Optional<HttpResponse> response = execute(request);
        return baseProcess(response, FetchMessagesResponse.class);
    }

    public Optional<UsersResponse> doFetchUsersRequest(String token) {
        FetchUsersRequest request = new FetchUsersRequest(serverUri, token);
        Optional<HttpResponse> response = execute(request);
        return baseProcess(response, UsersResponse.class);
    }

    public Optional<LogoutResponse> doLogoutRequest(String token) {
        LogoutRequest request = new LogoutRequest(serverUri, token);
        Optional<HttpResponse> response = execute(request);
        return baseProcess(response, LogoutResponse.class);
    }

    public Optional<SendMessageResponse> doSendMessageRequest(String token, String messageText) {
        internal.models.rest.SendMessageRequest model = new internal.models.rest.SendMessageRequest(messageText);
        SendMessageRequest request = new SendMessageRequest(serverUri, token, model);
        Optional<HttpResponse> response = execute(request);
        return baseProcess(response, SendMessageResponse.class);
    }
}
