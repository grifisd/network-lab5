package client.requests;

import common.constants.HttpMethod;
import common.constants.Queries;
import internal.utils.HeaderCreator;
import org.apache.http.Header;

import java.net.URI;

class FetchUsersRequest extends BaseRequest {
    private final String token;

    FetchUsersRequest(URI baseUri, String token) {
        super(baseUri);
        this.token = token;
    }

    @Override
    Header[] headers() {
        return new Header[]{HeaderCreator.authorization(token)};
    }

    @Override
    String path() {
        return Queries.users;
    }

    @Override
    HttpMethod method() {
        return HttpMethod.get;
    }

    @Override
    byte[] body() {
        return null;
    }

    @Override
    String query() {
        return null;
    }
}
